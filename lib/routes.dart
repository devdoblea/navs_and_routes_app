import 'package:flutter/material.dart';

import 'package:navs_and_routes_app/constants.dart';
import 'package:navs_and_routes_app/pages/home_page.dart';
import 'package:navs_and_routes_app/pages/second_page.dart';

class Router {
  static Route<dynamic> generatedRoute(RouteSettings settings) {
    switch (settings.name) {
      case homeRoute:
        return MaterialPageRoute(builder: (_) => Home() );
      case feedRoute:
      var data = settings.arguments as String;
        return MaterialPageRoute(builder: (_) => Feed(data) );
      default:
        return MaterialPageRoute(builder: (_) => Scaffold(
            body: Center(
              child: Text('Sin Ruta definida por ${settings.name}'),
            ),
          ) 
        );
    }
  }
}
