import 'package:flutter/material.dart';

import 'package:navs_and_routes_app/constants.dart';
import 'package:navs_and_routes_app/routes.dart';


void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: Router.generatedRoute,
      initialRoute: homeRoute,
    );
  }
}
