import 'package:flutter/material.dart';
import 'package:navs_and_routes_app/constants.dart';

class Feed extends StatelessWidget {
  final String data;

  Feed(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
			appBar: AppBar(
				title: Text("Segunda Pagina"),
				backgroundColor: Colors.deepOrange,
			),
			body: Container(
				child: Center(
					child: Column(
						mainAxisAlignment: MainAxisAlignment.center,
						children: <Widget>[
							IconButton(
								icon: Icon(
									Icons.home,
									color: Colors.blue
								),
								iconSize: 70.0,
								onPressed: () {
									//Navigator.pop(context);
                  Navigator.of(context).pushNamed(homeRoute);
								},
							),
							Text("Mensaje Recibido:"),
              Center(
                child: Text(
                  '< $data >',
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.deepPurple,
                  )
                ),
              ),
						]
					),
				),
			),
		);
  }
}
