import 'package:flutter/material.dart';
import 'package:navs_and_routes_app/constants.dart';

class Home extends StatelessWidget {
  @override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text("Home"),
				backgroundColor: Colors.deepOrange,
			),
			body: BodyHome(),
		);
	}
}

// Definir un formulario a tu gusto
class BodyHome extends StatefulWidget {
  BodyHome({Key key}) : super(key: key);

  @override
  _BodyHomeState createState() => _BodyHomeState();
}

// Definir el correspondiente estado de la clase
// Esta clase manejará los datos relativos al formulario.
class _BodyHomeState extends State<BodyHome> {

  /* Crear un TextController y usarlo para reciibir
   * el valor actual del campo de texto */
  final myController = TextEditingController();

  @override
  void dispose() {
    // Limpiar el controlador cuando despues que el widget lo utilice
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Escribe Algo...',
                  fillColor: Colors.white,
                  filled: true,
                ),
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.black,
                ),
                controller: myController,
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.favorite,
                color: Colors.redAccent
              ),
              iconSize: 70.0,
              onPressed: () {
                // Navigator.push(context, new MaterialPageRoute(
                // 	builder: secondRoute)
                // );
                Navigator.pushNamed(context, feedRoute, arguments: myController.text);
              },
            ),
            Text('...y haz "Tap" en el Corazón'),
          ]
        ),
      ),
    );
  }
}
