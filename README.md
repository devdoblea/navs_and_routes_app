Como usar "Navigators" y "Routes" en Flutter para redirigir a otras paginas
y ademas, enviando datos escritos en un textField por el usuario.

En este ejercicio, Intento coseguir una manera mas limpia de crear las rutas
de redireccion en flutter pues, es un tanto confuso cuando por ejemplo necesitas
redireccionar en una Material-App usando "Navigator.push" o usando "Navigator.pushNamed"

Aqui el enlace al video de donde inicié el ejemplo:

https://www.youtube.com/watch?v=RLyw-_MLLTo

Este video fue sencillo de seguir porque incluso es el ejercicio que viene
en las practicas de dev.flutter.com, pero me daba un error porque a pesar
de estar bien escrito (sin errores de semantica y cosas por el estilo), cuando
se ejecutaba decia que no podia conseguir el constructor de "MaterialPageRoute".
Investigando, aparentemente esta era la forma de hacerlo en anteriores de flutter;
ahora es necesario usar MateriaPageRoute para poder realizar los redireccionamientos
en una Material-App.
Una mejora que se puede hacer es tratar de recargar la pagina sin tener que cargar 
toda la pagina de nuevo, es decir, que solo cambie los datos que se utilizan y no toda
la pagina... (se ve feo la animacion de cuando pasa de una pagina a otra)

Aqui el tutorial de donde saqué el manejo del TextField para intercambiar datos 
entre paginas usando rutas limpias generadas

https://medium.com/flutter-community/clean-navigation-in-flutter-using-generated-routes-891bd6e000df

Aquí la Imagen de como quedó:

![Primera Pantalla](assets/images/Screenshot_2019-11-22-16-30-18.png)
![Escribiendo algo en Primera Pantalla](assets/images/Screenshot_2019-11-22-16-30-49.png)
![Resultado en la Pantalla Final con botón de retorno a la Pantalla Inicial](assets/images/Screenshot_2019-11-22-16-30-57.png)



Todo esto hecho en flutter 1.9..!
# navs_and_routes_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
